# Cobra

Simples do simples
```
package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

func Hello() *cobra.Command {
	return &cobra.Command{
		Use:   "hello [name]",
		Short: "Exibe: Olá, + <nome>",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("Olá, %s\n", args[0])
		},
	}
}

func main() {
	rootCmd := &cobra.Command{}

	rootCmd.AddCommand(Hello())

	rootCmd.Execute()
}
```