package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

func Hello() *cobra.Command {
	var name string

	cmd := &cobra.Command{
		Use:   "hello [name]",
		Short: "Exibe: Olá, + <nome>",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("Olá, %s\n", name)
		},
	}

	cmd.Flags().StringVarP(&name, "name", "n", "Mundo", "flag para concatenar com olá")

	return cmd
}

func main() {
	rootCmd := &cobra.Command{}

	rootCmd.AddCommand(Hello())

	rootCmd.Execute()
}
